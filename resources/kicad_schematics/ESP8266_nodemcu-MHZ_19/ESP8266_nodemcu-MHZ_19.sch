EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L project_symbols_library:PCF8574 U?
U 1 1 61223C5E
P 5600 1800
F 0 "U?" H 5878 1851 50  0000 L CNN
F 1 "PCF8574" H 5878 1760 50  0000 L CNN
F 2 "" H 5600 1800 50  0001 C CNN
F 3 "" H 5600 1800 50  0001 C CNN
	1    5600 1800
	1    0    0    -1  
$EndComp
Text GLabel 2300 3350 0    50   Input ~ 0
5V
Text GLabel 4950 1650 0    50   Input ~ 0
GND
Text GLabel 3900 3250 2    50   Input ~ 0
GND
Text GLabel 4950 1750 0    50   Input ~ 0
5V
Wire Wire Line
	4950 1850 4100 1850
Wire Wire Line
	4100 1850 4100 2150
Wire Wire Line
	4950 1950 4200 1950
Wire Wire Line
	4200 1950 4200 2050
Text Label 4550 1850 0    50   ~ 0
D2
Text Label 4400 1950 0    50   ~ 0
D1
$Comp
L project_symbols_library:MH-Z19 U?
U 1 1 61220E64
P 5500 3250
F 0 "U?" H 5500 3675 50  0000 C CNN
F 1 "MH-Z19" H 5500 3584 50  0000 C CNN
F 2 "" H 5500 3250 50  0001 C CNN
F 3 "" H 5500 3250 50  0001 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3250 4500 3250
Wire Wire Line
	4400 3350 5050 3350
Text GLabel 5950 3350 2    50   Input ~ 0
GND
Text GLabel 5950 3450 2    50   Input ~ 0
5V
Wire Wire Line
	4200 2050 3900 2050
Wire Wire Line
	4100 2150 3900 2150
Wire Wire Line
	4500 3250 4500 2250
$Comp
L esp12e:NodeMCU1.0(ESP-12E) U?
U 1 1 6121F4B4
P 3100 2650
F 0 "U?" H 3100 3737 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 3100 3631 60  0000 C CNN
F 2 "" H 2500 1800 60  0000 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2250 3900 2250
Wire Wire Line
	4400 2350 3900 2350
Wire Wire Line
	4400 2350 4400 3350
Text Label 4800 3250 0    50   ~ 0
D3
Text Label 4650 3350 0    50   ~ 0
D4
$EndSCHEMATC

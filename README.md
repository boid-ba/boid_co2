# Editing

Edit any markdown file, following the syntax.

Render (and edit) the documentation locally using:

```
gitbuilding serve  # Visit: http://localhost:6178/
```

Build the webpages:

```
gitbuilding build-html
```

Deploy to gitlab pages by `commmit`ting and `push`ing the changes.

# Viewing in gitlab pages

Visitar: https://boid-ba.gitlab.io/boid_co2/

# Syntax cheatsheet

* https://gitbuilding.io/usage/buildup/

Enlazar a otra pagina:

* `Print the components` es el texto que se muestra. Puede reemplazarse por un `.` para usar el titulo de la pagina (H1) en su `.md`.
* `CO2_sensor_main.md` es el nombre del markdown con el contenido de la pagina.
* `step` no sabemos jaja

```
[Print the components](CO2_sensor_main.md){step}
```

# Gitlab Pages and YAML help

* https://getpublii.com/docs/host-static-website-gitlab-pages.html
* https://stackoverflow.com/questions/31313452/yaml-mapping-values-are-not-allowed-in-this-context/31335106
* http://www.yamllint.com/

# Gitbuilding issues

Issues para reportar:

* ?

# Gitbuilding HTML customization

* general: https://gitbuilding.io/usage/buildconfsyntax/#customising-html-output
* head tag: https://gitlab.com/gitbuilding/gitbuilding/-/blob/master/gitbuilding/templates/full_page.html.jinja

## Syntax highlighting

Markdown is rendered with this module:

* https://python-markdown.github.io/
* https://python-markdown.github.io/reference/

It has a syntax highlighting extension:

* https://python-markdown.github.io/extensions/code_hilite/

Which can yse python pygments to make CSS styles:

    pygmentize -S default -f html -a .codehilite > pygment_styles.css

I included a line in `_templates/full_page.html.jinja`:

    <link href="pygment_styles.css" rel="stylesheet">

And added the `codehilite` extension to the gitbuilding source, at `gitbuilding/render.py`, like this:

    extensions = base_extensions + ["markdown_katex"] + ["codehilite"]

I hope it works!

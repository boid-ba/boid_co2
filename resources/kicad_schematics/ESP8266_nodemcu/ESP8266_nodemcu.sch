EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L esp12e:NodeMCU1.0(ESP-12E) U?
U 1 1 6121F4B4
P 3100 2650
F 0 "U?" H 3100 3737 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 3100 3631 60  0000 C CNN
F 2 "" H 2500 1800 60  0000 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC

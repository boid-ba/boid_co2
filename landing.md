# Sensores CO2

Instrucciones de ensamblado del sensor de CO2.

Encontrá pasos detallados para cada variante en estas páginas:

* [.](co2_variante1.md){step}
* [.](co2_variante2.md){step}
* [.](co2_variante3.md){step}
* Enviar datos a [ThingsBoard](thingsboard_telemetry.md)

La lista completa de materiales y componentes para construir todas estas variantes se encuentra en [esta página]{BOM}.

El repositorio de GitLab del proyecto sen encuentra en: https://gitlab.com/boid-ba/boid_co2/

Para **contribuir**, pueden:

* Escribir una respuesta a al hilo en el [foro de GOSH](https://forum.openhardware.science/t/documentacion-borrador-del-sensor-de-co2/3571).
* Crear un "issue" en el [repositorio de GitLab](https://gitlab.com/boid-ba/boid_co2/-/issues) del proyecto.
* Hacer un comentario público sobre la misma documentación (usando el menú desplgable a la izquierda, basado en hypothes.is)

![foto objeto](images/carcasa_ensamble.JPG)
![gente1](images/media/IMG_7476.JPG)
![gente2](images/media/IMG_7497.JPG)

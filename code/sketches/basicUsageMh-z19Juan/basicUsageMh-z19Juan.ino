#include <Arduino.h>
#include "MHZ19.h"
#include <SoftwareSerial.h>   // Remove if using HardwareSerial

#include <LiquidCrystal_I2C.h>

// set the LCD number of columns and rows


#define RX_PIN D3                                          // Rx pin which the MHZ19 Tx pin is attached to
#define TX_PIN D4                                          // Tx pin which the MHZ19 Rx pin is attached to
#define BAUDRATE 9600                                      // Device to MH-Z19 Serial baudrate (should not be changed)

int lcdColumns = 16;
int lcdRows = 2;
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);

MHZ19 myMHZ19;                                             // Constructor for library
SoftwareSerial mySerial(RX_PIN, TX_PIN);                   // (Uno example) create device to MH-Z19 serial

unsigned long getDataTimer = 0;

void setup()
{
  Serial.begin(9600);                                     // Device to serial monitor feedback

  mySerial.begin(BAUDRATE);                               // (Uno example) device to MH-Z19 serial start
  myMHZ19.begin(mySerial);                                // *Serial(Stream) refence must be passed to library begin().

  myMHZ19.autoCalibration();                              // Turn auto calibration ON (OFF autoCalibration(false))
  // initialize LCD
  lcd.init();
  // turn on LCD backlight
  lcd.backlight();

}

void loop()
{
  if (millis() - getDataTimer >= 2000)
  {
    
    /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even
      if below background CO2 levels or above range (useful to validate sensor). You can use the
      usual documented command with getCO2(false) */
    int CO2;
    CO2 = myMHZ19.getCO2();                             // Request CO2 (as ppm)
    String CO2Str = String(CO2);
    
    Serial.print("CO2 (ppm): ");
    Serial.println(CO2Str);

    int8_t Temp;
    // Request Temperature (as Celsius)
    Temp = myMHZ19.getTemperature();  
    String TempStr = String(Temp);
    
    Serial.print("Temperature (C): ");
    Serial.println(TempStr);

    lcd.clear();
    
    lcd.setCursor(0, 0);

    String CO2Message = "CO2(ppm):";
    CO2Message += CO2Str;
    if(CO2 > 800){
        CO2Message += " :o";
      }
    lcd.print(CO2Message);

    
    lcd.setCursor(0, 1);

    String TempMessage = "Temp(C):";
    TempMessage += TempStr;
    lcd.print(TempMessage);
    
    getDataTimer = millis();
  }
}

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L esp12e:NodeMCU1.0(ESP-12E) U?
U 1 1 6121F4B4
P 3100 2650
F 0 "U?" H 3100 3737 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 3100 3631 60  0000 C CNN
F 2 "" H 2500 1800 60  0000 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
$Comp
L project_symbols_library:PCF8574 U?
U 1 1 61223C5E
P 5600 2400
F 0 "U?" H 5878 2451 50  0000 L CNN
F 1 "PCF8574" H 5878 2360 50  0000 L CNN
F 2 "" H 5600 2400 50  0001 C CNN
F 3 "" H 5600 2400 50  0001 C CNN
	1    5600 2400
	1    0    0    -1  
$EndComp
Text GLabel 2300 3350 0    50   Input ~ 0
5V
Text GLabel 4950 2250 0    50   Input ~ 0
GND
Text GLabel 3900 3250 2    50   Input ~ 0
GND
Text GLabel 4950 2350 0    50   Input ~ 0
5V
Wire Wire Line
	4950 2450 4100 2450
Wire Wire Line
	4100 2450 4100 2150
Wire Wire Line
	4100 2150 3900 2150
Wire Wire Line
	4950 2550 4200 2550
Wire Wire Line
	4200 2550 4200 2050
Wire Wire Line
	4200 2050 3900 2050
Text Label 4400 2450 0    50   ~ 0
D2
Text Label 4400 2550 0    50   ~ 0
D1
$EndSCHEMATC

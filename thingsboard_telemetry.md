# Monitoreo remoto con ThingsBoard

Introducción breve:

"ThingsBoard" es una plataforma web de software libre que sirve para monitorear e interactuar con "cosas" conectadas a internet.

Una de esas "cosas" puede ser un microcontrolador, como los que usamos en este proyecto, que envía datos sobre concentración de CO2.

Esos datos son recibidos y almacenados por ThingsBoard, y quedan disponibles para que los consultemos luego, sin tener que estar colectando los datos a mano.

## Tutoriales

Lo que escibo acá es algo muy breve para tener de referencia, pero hay muy buenos tutoriales en internet.

Los dos tutoriales más clave son:

* ESP8226: https://thingsboard.io/docs/samples/esp8266/temperature/
* Tablero / dashboard: https://thingsboard.io/docs/user-guide/dashboards/

## Instancia de ThingsBoard

Una parte de importante de ThingsBoard es su sitio web, con el que los usuarios interactuamos normalmente.

Podemos alojar el sitio web en cualquier computadora, pero para empezar lo más fácil es usar el sitio _demostrativo_ disponible acá:

* https://thingsboard.io/docs/user-guide/live-demo/

Hay que entrar a demo.thingsboard.io y hacerse una cuenta.

## Agregar un dispositivo

Primero agregamos un dispositivo a ThingsBoard.

Lo más importante de este paso es configurar el tipo de comunicación como "MQTT" y obtener un "token".

![](images/thingsboard/Screenshot_20210822_015826.png)
![](images/thingsboard/Screenshot_20210822_015840.png)
![](images/thingsboard/Screenshot_20210822_015922.png)
![](images/thingsboard/Screenshot_20210822_015945.png)

## Configurar el microcontrolador

### Librerias

Necesitamos una librería que nos haga la vida fácil.

La librería `PubsubClient` para Arduino se encarga de enviar y recibir mensajes MQTT, dejando en nuestras manos la configuración y el uso.

Se puede instalar usando el menú usual de Arduino IDE.

### Código

La configuracion se hace al pincipio del programa con:

```
// Librería
#include <PubSubClient.h>

// Credenciales
#define ssid                "SSID de tu red wifi"
#define WIFI_PASSWORD       "CONTRASEÑA de tu red wifi"
#define THINGSBOARD_TOKEN   "el TOKEN que generamos antes"

// ThingsBoard server address or URL
char thingsboardServer[] =  "demo.thingsboard.io";
```

En los chips basados en ESP8226 después hacemos lo siguiente:

```
// Librería de herramientas específicas del ESP8226
#include <ESP8266WiFi.h>

// Instanciar la clase "PubSubClient"
WiFiClient wifiClient;
PubSubClient client(wifiClient);
```

Dentro del `void setup()` del programa de Arduino hay que incluir:

```
  // Para habilitar el WiFi
  InitWiFi();

  // Para configurar el server de ThingsBoard
  client.setServer( thingsboardServer, 1883 );
```

La función `InitWiFi` se debe definir (por ejemplo, más abajo en el programa) y se encarga de asegurar de estar conectado al WiFi antes de hacer otra cosa
Pueden encontrar el resto del código [aquí](code/ESP8266_thingsboard.io_test.zip). Ese código es para medir temperatura y mandar el dato a ThingsBoard.

En muchos casos preferiríamos que el resto del programa se ejecute igual, aunque no haya wifi.

En ese caso, deberíamos programar una lógica que no bloquee las mediciones (u otras funciones) si falla internet o el WiFi.
Hay ejemplos en internet que pueden encontrarse como "non-blocking", que típicamente lo que hacen en chequear cada tanto si volvió el internet
(en vez de quedarse esperando, chequeando sin parar desesperadamente :P).

Incluyo un ejemplo de telemetría "non-blocking" que puede encontrarse en [este repo](https://gitlab.com/naikymen/yeast_wheel/-/tree/master/code/incubadora_rotor_pid).
Ese código es para medir temperatura y frecuencia de rotacion, aplicar control PID sobre ellas, y mandar el datos a ThingsBoard.

## Mostrar datos en ThingsBoard

Asumiendo que todo fue bien, ahora volveríamos a ThingsBoard para crear un "tablero" (o _dashboard_) donde se muestren nuestros datos.

Algunas capturas para orientar como se hace eso:

![](images/thingsboard/Screenshot_20210822_024724.png)
![](images/thingsboard/Screenshot_20210822_024801.png)
![](images/thingsboard/Screenshot_20210822_025001.png)
![](images/thingsboard/Screenshot_20210822_025104.png)
![](images/thingsboard/Screenshot_20210822_025120.png)

Hay varios tutoriales sobre cómo hacer todas estas cosas.

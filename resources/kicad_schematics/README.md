Hay mil (varias) plaquitas con ESP8226, y su forma y distribución de pines puede ser bastante diferente.

Ver: https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/

Nosotros aparentemente tenemos el chip `ESP8266 12-E` en el kit de `NodeMCU`.

En ese sitio también se puede encontrar el pinout delmismo chip pero en la plaquita conocida como `Wemos D1 Mini`.

La librería de KiCAD para el ESP8226 12E que usé es esta: https://github.com/jdunmire/kicad-ESP8266

Para incluir esa librería:

* Preferences -> Manage symbol libraries -> Project specific libraries
  * Luego con el boton "+" se agrega una entrada y en el library path hay que apuntar al archivo ".lib"
* Preferences -> Manage footprint libraries -> Project specific libraries
  * Luego con el boton "+" se agrega una entrada y en el library path hay que apuntar al directorio ".pretty"

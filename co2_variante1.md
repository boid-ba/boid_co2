---
Details:
    Thumbnail: images/carcasa_ensamble.JPG
    Time: 1 hour
    Difficulty: Easy
    Skills:
      - Cable connecting
      - Screw-driving
      - Button pressing
---

# Sensor CO2 - Variante Principal

Sensor de CO2 basado en MHZ19 y el microcontrolador ESP8266 (marcado como ESP12E).

{{BOM}}

## Probar el microcontrolador {pagestep}

- Abrir el kit y encontrar el microcontrolador [NodeMCU ESP8266 12E](Parts.yaml#NodeMCU_ESP8266){qty: 1} microcontroller, y el [cable micro USB](Parts.yaml#cable_usb_micro){qty: 1}.

![](images/NodeMCU_ESP8266-top.jpg)
![](images/NodeMCU_ESP8266-bot.jpg)
![](images/cable_usb_micro.jpg)

- Conectar el microcontrolador a la computadora, y abrir el Arduino IDE (si no lo tienen ya, deben [instalarlo](https://www.arduino.cc/en/software) antes).

### Referencias

Guías en las que nos basamos:

* Pinout NodeMCU ESP12E: https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/
* Pinout NodeMCU ESP32: https://randomnerdtutorials.com/esp32-esp8266-i2c-lcd-arduino-ide/

### Instalar la plataforma ESP8266 en el Arduino IDE

Primero hay que instalar los controladores del microcontrolador ESP8266  el Arduino IDE.

Primero hay que ir a _File -> Preferences_, y en el campo "_Additional Boards Manager URLs_" pegar lo siguiente:

    http://arduino.esp8266.com/stable/package_esp8266com_index.json

Luego, ir a "_Tools -> Board -> Boards Manager..._", y con la ventanita que aparece buscar e instalar el ESP8266.

Finalmente, hay que seleccionar el board en "_Tools -> Boards -> ESP8266 Boards -> NodeMCU 1.0 (ESP-12E Module)_".

![](images/boards_manager-additional.png)
![](images/boards_manager-install_ESP8266.png)
![](images/boards_manager.png)

Con el microcontrolador conectado a la computadora con el cable USB, ahora deberíamos ver que en el menú "_Tools -> Port_" debería aparecer una entrada nueva para el microcontrolador,
con un nombre similar a "_/dev/ttyUSB0_". En windows los puertos tienen nombres como "_COM5_", si hay varios, se puede ir probando cada uno hasta encontrar el que funciona.

![](images/select_port2.png)

Basamos la configuración del [Arduino IDE](Parts.yaml#arduino_ide){Qty: 1, cat:tool} para usar ESP8266 en [estas instrucciones](https://www.instructables.com/Programming-ESP8266-ESP-12E-NodeMCU-Using-Arduino-/) (del paso 2 al 5).

### Dificultades frecuentes

> No aparece el puerto del microcontrolador en la lista de puertos del Arduino IDE.

Si esto sucede, puede ser que el cable USB no sirva para transmitir datos, y haga falta probar con otro.

También puede ser que falte instalar "drivers". Esta guía está probada en una compitadora con sistema operativo GNU/Linux.

Si estás usando Windows, es posible que debas instalar drivers adicionales para poder ver la entrada nueva en el menú de puertos.
También es posible que necesites instalar otra versión del Arduino IDE, a nosotros nos falló la versión 1.8.5 y resolvimos el
problema instalando la versión 1.8.15.

* Drivers: [silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)
* Otros drivers para probar si no alcanza con los anteriores: [github.com/nodemcu/nodemcu-devkit/tree/master/Drivers](https://github.com/nodemcu/nodemcu-devkit/tree/master/Drivers)
* Fuente: [github.com/esp8266/Arduino/issues/3551](https://github.com/esp8266/Arduino/issues/3551)

### Primer código de prueba

- Si todo sale bien, deberían ver opciones para `ESP8266` habilitadas en el menú `Tools > Board`.
Hay que seleccionar el `NodeMCU 1.0 (ESP-12E Module)`.
- Copiar el siguiente código y pegarlo en el Arduino IDE:

```
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
  Serial.println("Hola Boid!");
  digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
  // but actually the LED is on; this is because
  // it is active low on the ESP-01)
  delay(1000);                      // Wait for a second
  digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  delay(2000);                      // Wait for two seconds (to demonstrate the active low LED)
}
```

- Seleccionar el "puerto" al que está conectado el NodeMCU:

![](images/select_port2.png)

- Subir el sketch al microcontrolador con el botón "_Upload_":

![](images/upload_sketch.png)

- Verificar en el _serial monitor_ que aparezca el mensaje `"Hola Boid!"` (asegurar que el baudrate este a 115200). Además, el LED azul integrado a la plaquita deberia encenderse cada 2 segundos.

![](images/serial_monitor.png)
![](images/blink.gif)

Congrats! Ahora podés trabajar con el microcontrolador.

---

## Probar LCD display {pagestep}

### Referencias

Guías en las que nos basamos:

* https://www.losant.com/blog/how-to-connect-lcd-esp8266-nodemcu
* https://lastminuteengineers.com/i2c-lcd-arduino-tutorial/

### Preprar LCD display library

Instalar la library "_LiquidCrystal I2C_" de Marco Shwartz, usando el library manager del Arduino IDE:

- [LiquidCrystal_I2C](https://github.com/johnrickman/LiquidCrystal_I2C)

Más adelante quizás les diga que es "incompatible" con ES8266 o algo así, y está hecha para otra arquitectura (_avr_), pero no pasa nada; funciona igual.

![](images/library_install-liquidcrystalI2C.png)

### Conexiones LCD display

Usaremos un [I2C 16x2 LCD display](Parts.yaml#LCD1602_16x2_I2c){qty:1} LCD, controlado con el protocolo I2C, y haremos las conexiones usando [cables dupont hembra-hembra](Parts.yaml#cableDupontHH){qty: 2}, [cables dupont hembra-macho](Parts.yaml#cableDupontHM){qty: 2} y un [mini protoboard](Parts.yaml#mini_protoboard){Qty:1}.

A continuación se muestran las conexiones necesarias para que el microcontrolador se comunique con el display LCD con el protocolo "_I2C_":

| Pin I2C LCD | Pin NodeMCU ESP12E |
|---------|-------------|
| GND     | GND         |
| VCC     | VIN         |
| SDA     | GPIO 4 (D2) |
| SCL     | GPIO 5 (D1) |

Noten que VCC y GND se conectan a través del protoboard, ya que serán compartidos con los pines correspondientes del sensor de CO2 (ver fotos).

![](resources/kicad_schematics/exported_drawings/ESP8266_nodemcu-LCD_driver.png)
![](images/library_test-LCD-wiring1.JPG)

> Nota: ver diagrama editable en KiCAD: `resources/kicad_schematics/ESP8266_nodemcu-LCD_driver/ESP8266_nodemcu-LCD_driver.sch`

Nosotros usamos el mini protoboard solo para Vcc, GND y los pines del MHZ19. El pin `Vin` en el microcontrolador provee 5V (del cable USB), y va conectado al `VCC` del LDC (y del sensor) para alimentarlo eléctricamente.

Si usan un protoboard de [tamaño normal](https://en.wikipedia.org/wiki/Breadboard#/media/File:400_points_breadboard.jpg) (en vez del "mini" que se ve en la foto), pueden montar el microcontrolador sobre el mismo, dejando pines a cada costado. Para hacerlo hay que alinearlo, y hacer fuerza hasta que entre del todo (puede ser que cueste, y que haga falta apretar fuerte sobre los pines).

Los pines de I2C en el Nodemcu V1 son D1 (SCL) y D2 (SDA), pero [se pueden cambiar](https://github.com/esp8266/Arduino/issues/887#issuecomment-147862216), si hiciera falta.

### Encontrar el I2C "address" del LCD display

I2C es un modo de conectar varios dispositivos compartiendo los mismos (dos) cables de datos.

Entonces para poder diferenciar a los dispositivos, se asigna una "dirección" a cada uno.

¿Cuál es la dirección I2C de nuestro display? Necesitamos averiguarla para que el microcontrolador se cominuque con este display.

Para saberlo, subimos el siguiente sketch al microcontrolador, y luego abrimos el serial monitor:

```
/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

#include <Wire.h>
 
void setup() {
  Wire.begin();
  Serial.begin(115200);
  Serial.println("\nI2C Scanner");
}
 
void loop() {
  byte error, address;
  int nDevices;
  Serial.println("Scanning...");
  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
      nDevices++;
    }
    else if (error==4) {
      Serial.print("Unknow error at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0) {
    Serial.println("No I2C devices found\n");
  }
  else {
    Serial.println("done\n");
  }
  delay(5000);          
}
```

> Nota: el "baudrate" seleccionado en el serial monitor (abajo a la derecha) tiene que coincidir con el número en el setup() del sketch para poder ver los mensajes. En este caso es 115200.

Deberian esperar ver algo así en el serial monitor (ver foto): `I2C device found at address 0x27`

![](images/library_test-I2Caddrfind.png)

Ese "`0x27`" es la dirección I2C del display. En su caso puede ser un poco diferente.

### Mostrar algo de texto en el display

Subiendo este sketch:

```
/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

// Incluir la library que bajamos para controlar el display LCD por I2C.
#include <LiquidCrystal_I2C.h>

// Especificar el número de filas y columnas que tiene nuestro display:
int lcdColumns = 16;
int lcdRows = 2;

// Configurar la dirección I2C del LCD, y el número de filas y columnas que tiene.
// si no conocés la dirección, usa el sketch anterior del tutorial para averiguarlo.
// También hay un ejemplo en el Arduino IDE para eso.
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);  

void setup(){
  // Inicializar el display
  lcd.init();

  // Encender la luz del diplay (la retroiluminación)
  lcd.backlight();
}

void loop(){
  // Poner el cursor al principio de primera línea (coordenada 0,0)
  lcd.setCursor(0, 0);
  // Imprimir un mensaje
  lcd.print("Hello, World!");
  // Esperar 1 segundo
  delay(1000);

  // Limpiar el display
  lcd.clear();

  // Poner el cursor al principio de la segunda línea (coordenada 0,1)
  lcd.setCursor(0,1);
  // Imprimir un mensaje
  lcd.print("Hello, World!");
  // Esperar 1 segundo
  delay(1000);

  // Limpiar el display
  lcd.clear(); 
}
```

> Nota: recuerden reemplazar el valor del "address" en la línea `LiquidCrystal_I2C` por el valor que hayan obtenido ustedes. En este caso es "`0x27`".

Deberían ver algo así:

![hola](images/library_test-LCDhello.JPG)

Pueden jugar a cambiarle el mensaje de `"Hello, World!"` a otra cosa:

* `"Hola CO2, te estamos sensando"`
* `"Keacés bb ;)"`
* etc. :)

---

## Probar el sensor MHZ-19 {pagestep}

### Libraries

La library que necesitamos se encuentra disponible en el gestor de librerías del Arduino IDE.

Se puede instalar con el menú "_Sketch -> Include library -> Manage libraries..._" o "_Tools -> Manage libraries..._",
y buscando "MH-Z19" (instalamos la de [Jonathan Dempsey](https://github.com/WifWaf/MH-Z19)).

### Conexiones

Este sensor se puede comunicar con el arduino por un protocolo de "comunicación serial".

Este modo de comunicación requiere dos pines en cada uno de los dispositivos que se conectan: un "Tx" (pin de transmisión de mensajes) y un "Rx" (pin de recepción de mensajes). Haremos las conexiones usando [cables dupont hembra-hembra](Parts.yaml#cableDupontHH){qty: 2} y [cables dupont hembra-macho](Parts.yaml#cableDupontHM){qty: 2}.

Como regla general, el pin `TX` de un dispositivo se conecta con el pin `RX` del otro (digamos que "se cruzan").

Las conexiones para nuestro caso se muestran a continuación:

| Pin MH-Z19 | Pin NodeMCU ESP12E |
|---------|------------------|
| GND     | GND              |
| VCC     | VIN              |
| TX      | D3 (Software RX) |
| RX      | D2 (Software TX) |

Noten que VCC y GND se conectan a través del protoboard, ya que son compartidos con los pines correspondientes del dis (ver fotos).

![](resources/kicad_schematics/exported_drawings/ESP8266_nodemcu-MHZ_19.png)
![](images/library_test-LCD-wiring1.JPG)

> Nota: en el esquema de KiCAD no aparece explícitamente el protoboard que se ve en la foto. Está implícito en los pines marcados con "tags" `GND` y `5V`.

### Código para pruebas

Probar con el sketch de ejemplo que viene con la libreria, llamado "Basic Usage".

Se puede acceder a él a través del menú "_File -> Examples -> MH-Z19 -> BasicUsage_".

La parte importante del sketch de ejemplo es la siguiente, donde deberíamos poner los números de pin correctos:

```
#define RX_PIN D3
#define TX_PIN D4
```

Una vez reemplazados los valores, y subido el código al microcontrolador, abrimos el _Serial monitor_ del Arduino IDE y esperamos ver mensajes con la concentration de CO2 en "ppm" (partes por millón).

![](images/library_test-MHZ19.png)

> Nota: Una vez más, para ver correctamente los mensajes, el baudrate en el sketch (9600) tiene que coincidir con el que aparece en el serial monitor.

#### Aclaraciones sobre SoftwareSerial

Como el ejemplo usa "software serial", hay flexibilidad en las combinaciones de pines que podemos usar como Tx y Rx en el NodeMCU ESP12E.
En [este ejemplo](https://diyprojects.io/publish-co2-concentration-mh-z19-sensor-domoticz-arduino-code-compatible-esp32-esp8266/) usan D7 y D6 en vez de D3 y D4.

Además en [este ejemplo](http://www.jopapa.me/CO2.html) parecen usar "hardware serial", con los pines `TX` y `RX` que se ven marcados sobre la plaquita del microcontrolador.

Por ahora nosotros nos quedaremos con los pines `D3` para RX y `D4` para TX del NodeMCU.

Si aparece un error de "timeout", una solución puede ser revisar que estén bien hechas las conexiones. El tema de Rx/Tx puede ser confuso.

### Calibración

1. Poner el sensor al aire libre (sobre la ventana por ejemplo).
    - Preferimos esta poruqe parece mejor documentada.
2. Esperar 20 minutos, para que se estabilice.
3. Subir el sketch de ejemplo "Calibration" de la librería que usamos antes (la de Jonathan).
    - Modificar los pines igual que antes también (para que use D3 y D4).
    - Comentar las líneas de la "espera".
    - O bien pueden usar [este sketch](code/MHZ19-calibration.zip).
4. Revisar los mensajes en el serial monitor del Arduino IDE.
    - Deberíamos ver PPMs cercanos a 400. 

![](images/library_test-MHZ19calib.png)
![](images/library_test-MHZ19calib_cat.JPG)

Para la posteridad, esta es la info de nuestro módulo MHZ19:

```
Firmware Version: 05.17
Range: 5000
Background CO2: 403
Temperature Cal: 40
ABC Status: OFF
```

> Nota: la info viene del sketch de ejemplo "RetrieveDeviceInfo" en la library de Jonathan.

### Referencias

Libraries:

- La de Jonathan Dempsey: https://www.arduino.cc/reference/en/libraries/mh-z19/
- Las que usa Jorge Aliaga (posiblemente): 
    - Sketch de calibración: https://github.com/jlaliaga/Medidor-de-CO2/blob/V2/MHZ19_zerocalib_LCD.ino
    - https://github.com/crisap94/MHZ19
    - https://github.com/AlexGyver/MeteoClock/tree/master/libraries/mhz19_uart

Conexiones:

- https://diyprojects.io/publish-co2-concentration-mh-z19-sensor-domoticz-arduino-code-compatible-esp32-esp8266/
- https://robertwisbey.com/carbon-dioxide-levels-using-the-mh-z19b-sensor/
- https://www.winsen-sensor.com/d/files/PDF/Infrared%20Gas%20Sensor/NDIR%20CO2%20SENSOR/MH-Z19%20CO2%20Ver1.0.pdf

---

## Mostrar PPMs en el display LCD {pagestep}

En el repositorio hay sketches de ejemplo para esto, que usan las mismas conexiones que armamos en este tutorial. Ver sketches en [el repo](https://gitlab.com/boid-ba/boid_co2/-/tree/master/code/sketches/).

1. Calibrar el sensor siguiendo todos los pasos anteriores.
2. Subir [este sketch](code/sketches/basicUsage-conPantallita.zip) al microcontrolador.
3. Observar el display LCD, y verificar que los datos se muestran :) 

![](images/library_test-MHZ19-LCDisplay.jpg)

## Preparar carcasa {pagestep}

1. Abrimos la [caja de luz](Parts.yaml#caja_cpt2){qty:1}.
2. Pasamos el display para que quede debajo de la ventanita, y la cerramos.
3. Usando un [cuchillo caliente]{Qty: 1, cat:tool}, hacemos un agujero más ancho detrás de la caja, para poder pasar el cable micro USB.
4. Pasamos el cable micro USB por el agujero, y lo conectamos al microcontrolador.
5. Metemos todo dentro cuidadosamente, encajamos la tapa, y apretamos los tornillos con el [destornillador]{qty: 1, cat: tool}.

![agujerear](images/carcasa_agujero.JPG)
![agujerear](images/carcasa_ensamble.JPG)

> **Alternativa al "cuchillo caliente"**: usar el soldador de estaño _solamente_ si se lo limpia muy bien después, con virulana y pasta de soldar, para no arruinarlo.

### Referencias

* https://www.thingiverse.com/search?q=mh-z19&dwh=705ddd60a275ec2&type=things&sort=relevant (no usada)

## Uso

> Es importante que se haya hecho la calibración correctamente en los pasos anteriores, antes de usar el aparato.

1. Conectar el cable USB a la [fuente USB](Parts.yaml#fuente_USB){qty: 1}.
2. La caja debería quedar "parada" para no obstruir el paso de aire por las ranuras detrás.
3. Un emoticón indicador ":o" aparecerá cuando se mida más de 800 ppm de CO2, indicando la necesidad de ventilar.

## Mejoras

En el kit se incluyen partes para mejorar el dispositivo:

* LED RGB como "semáforo".
* Botón para generar una versión que se calibre automáticamente.
* Pila de litio recargable, que le permita funcionar frente a fallos en el suministro eléctrico.
* Función WiFi en el microcontrolador, para enviar datos periódicamente o consultarlos.

## Contacto

Si necesita consultar sobre estas mejoras, o por otro motivo, por favor contáctenos por alguno de los siguientes medios:

* Creando un "issue" en nuestra [página de GitLab](https://gitlab.com/boid-ba/boid_co2/-/issues).
* Comentando en [el espacio Boid](https://forum.openhardware.science/c/latino-america-y-caribe/boid/49) del foro de GOSH.


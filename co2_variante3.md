---
Details:
    Thumbnail: images/carcasa_ensamble.JPG
    Time: 1 hour
    Difficulty: Easy
    Skills:
      - Cable connecting
      - Screw-driving
      - Button pressing
---

# Sensor CO2 - Variante MQ135

Sensor de CO2 basado en MQ135 y el microcontrolador ESP8266 (marcado como ESP12E).

{{BOM}}


## Probar el microcontrolador {pagestep}

TO DO: importar de la variante 1.

## Probar LCD display {pagestep}

TO DO: importar de la variante 1.

## Probar el sensor MQ135 con LCD display {pagestep}

Usamos de base esta guia: https://how2electronics.com/iot-air-quality-index-monitoring-esp8266/

### Preprar library MQ135

1. Instalar la library `MQ135` en el Arduino IDE
2. Descargar: https://github.com/GeorgK/MQ135/archive/refs/heads/master.zip
3. Agregar la library al Arduino IDE usando el menú `Sketch -> Include Library -> Add .ZIP Library...` y seleccionar el archivo bajado.
    - La library se incluye en un sketch con la linea `#include "MQ135.h"` al inicio del programa (ver ejemplo más abajo).

### Subir sketch

Una vez instalada la library necesaria, y con el ESP8226 conectado correctamente, subir el siguiente sketch de prueba:

```Cpp
#include "MQ135.h"

void setup(){
  Serial.begin(115200);
}


void loop(){
    MQ135 gasSensor = MQ135(A0);
    float air_quality = gasSensor.getPPM();
    Serial.print("Air Quality: ");
    Serial.print(air_quality);
    Serial.println("  PPM");
    Serial.println();

    Serial.println("Waiting...");

    delay(2000);
}
```

Abrir el _serial monitor_ (a baud rate 115200) y esperar a ver los mensajes del programa.

Todavía no conectamos el sensor, pero si hay mensajes, está bien.

Finalmente, desconectar el cable USB de la PC.

### Conexiones MQ135

Luego conectar el sensor [MQ135](Parts.yaml#MQ135){qty:1} al protoboard usando tres [cables jumper HM](Parts.yaml#cableDupontHM){qty:3}.

Finalmente conectar el protoboard al microcontrolador con otros tres [cables jumper HM]{qty:3}.

Las conexiones deberian respetar la siguiente tabla:

| Pin ESP8226 | Pin MQ135 |
|-----|-----|
| Vin | VCC |
| GND | GND |
| A0  | A0  |

> TO DO: Agregar diagrama fritzing o equivalente.

Este sensor usa más corriente de la que puede proporcionar el puerto USB de una PC, así que habrá que usar una [fuente USB](Parts.yaml#fuente_USB){qty:1} de pared.

Conectar el cable usb a la fuente (y la fuente a la toma), y esperar unos segundos hasta ver los mensajes correctos en el display LCD.

### Calibración

## Mostrar PPMs en el display LCD {pagestep}

> TO DO

## Preparar carcasa {pagestep}

> TO DO

## Comparar con MHZ19 {pagestep}

> TO DO

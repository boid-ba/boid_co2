 
Convertir MP4 a GIF y reducir tamaño:

 convert -delay 2 doc_2021-07-07_14-29-23.mp4 -coalesce -layers optimize -loop 0 blink.gif
 convert -size 480x464 blink.gif -resize 128x128 smaller.gif
 convert -size 864x480 led.gif -resize 216x120 smaller.gif
